package data

import Settings.DeckSettings
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import com.google.protobuf.InvalidProtocolBufferException
import java.io.InputStream
import java.io.OutputStream

object SettingsSerializer : Serializer<DeckSettings> {

    override val defaultValue: DeckSettings = DeckSettings.getDefaultInstance()

    override suspend fun readFrom(input: InputStream): DeckSettings {
        try {
            return DeckSettings.parseFrom(input)
        } catch (exception: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read proto", exception)
        }
    }

    override suspend fun writeTo(
        protoDeckSettingsStore: DeckSettings,
        output: OutputStream
    ) = protoDeckSettingsStore.writeTo(output)

}