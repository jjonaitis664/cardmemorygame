package data

import Settings.DeckSettings
import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import java.io.IOException

/*
val DATA_STORE_FILE_NAME = "deck_settings.pb"
*/
/*
val Context.protoDataStore: DataStore<DeckSettings> by dataStore(
    fileName = DATA_STORE_FILE_NAME,
    serializer = SettingsSerializer
)*/

class ProtoSettingsRepository(
    private val protoDataStore: DataStore<DeckSettings>,
    ) {

    private val TAG: String = "ProtoSettingsRepository"

    val deckSettingsFlow: Flow<DeckSettings> = protoDataStore.data
        .catch { exception ->
            if (exception is IOException) {
                Log.e(TAG, "Error reading sort order preferences.", exception)
                emit(DeckSettings.getDefaultInstance())
            } else {
                throw exception
            }
        }

    suspend fun updateDeckAmountOfCards(ammountOfCards: Int) {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().setAmountOfCards(ammountOfCards).build()
        }
    }

    suspend fun clearDeckAmountOfCards() {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().clearAmountOfCards().build()
        }
    }

    suspend fun updateSuits(suit: String) {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().setSuits(suit).build()
        }
    }

    suspend fun clearSuits() {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().clearSuits().build()
        }
    }

    suspend fun updateCardsDivision(cardDivision: Boolean) {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().setCardsDivision(cardDivision).build()
        }
    }

    suspend fun clearCardDivision() {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().clearCardsDivision().build()
        }
    }

    suspend fun updateCardNumberOffset (cardNumberOffset: Int) {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().setCardNumberOffsetValue(cardNumberOffset).build()
        }
    }

    suspend fun clearCardNumberOffset() {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().clearCardNumberOffset().build()
        }
    }

    suspend fun updateStartingCardNumber(startingCardNumber: Int) {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().setStartingCardNumberValue(startingCardNumber).build()
        }
    }

    suspend fun clearStartingCardNumber() {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().clearStartingCardNumber().build()
        }
    }

    suspend fun clearDataStore() {
        protoDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().clear().build()
        }
    }

}