package com.example.cardmemoryapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import UI.Navigation
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi


// What kind of suit there is
// Dont show last card(You should be able to find what card is missing)
// How many cards
// what kind of cards(A, 2 , 3 .etc)

// When training with deck you should be able to input value of card
// or chose grid based and check them then
// After training you should see griid based corect and incorect cards placements


@ExperimentalAnimationApi
class MainActivity : ComponentActivity() {
    @ExperimentalMaterialApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Navigation()
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Navigation()
}