package UI

import android.app.Activity
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.example.cardmemoryapp.R
// R below only has general android resources and not my apps resource
//import androidx.compose.ui.R
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import UI.composeElements.mainMenuButton

/*
class MenuScreen {
}*/

@Composable
fun MainMenuScreen(navController: NavController) {


    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Text(
            text = stringResource(id = R.string.main_menu_name),
            modifier = Modifier.padding(vertical = 10.dp),
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            fontSize = 35.sp
        )

        mainMenuButton(navController = navController,
            route = ScreenRoutes.DealtCardScreen.route,
            buttonName = stringResource(R.string.dealt_cards_option))

        mainMenuButton(navController = navController,
            route = ScreenRoutes.SavedDecksScreen.route,
            buttonName = stringResource(R.string.saved_cards_option))

        mainMenuButton(navController = navController,
            route = ScreenRoutes.SettingsScreen.route,
            buttonName = stringResource(R.string.settings_option))

        val activity = LocalContext.current as Activity
        //TODO should kill the app and not minimize it
        Button(
            onClick = {activity.finish()},
            modifier = Modifier
                .requiredSize(250.dp, 100.dp)
                .padding(vertical = 10.dp)
        ) {
            Text(stringResource(R.string.quit_app_option))
        }
    }
}