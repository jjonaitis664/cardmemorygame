package UI

sealed class ScreenRoutes(val route: String) {

    object MainMenuScreen : ScreenRoutes("/home/sm/Desktop/sm/kotlin/cardMemoryApp/app/src/main/java/UI/MainMenuScreen.kt")
    object DealtCardScreen : ScreenRoutes("/home/sm/Desktop/sm/kotlin/cardMemoryApp/app/src/main/java/UI/DealtCardScreen.kt")
    object SavedDecksScreen : ScreenRoutes("/home/sm/Desktop/sm/kotlin/cardMemoryApp/app/src/main/java/UI/SavedDecksScreen.kt")
    object SettingsScreen : ScreenRoutes("/home/sm/Desktop/sm/kotlin/cardMemoryApp/app/src/main/java/UI/SettingsScreen.kt")

}