package UI.composeElements

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable

fun mainMenuButton(navController: NavController, route: String, buttonName: String) {
    Button(
        onClick = {navController.navigate(route)},
        modifier = Modifier
            .requiredSize(250.dp, 100.dp)
            .padding(vertical = 10.dp)
    ) {
        Text(buttonName)
    }
}