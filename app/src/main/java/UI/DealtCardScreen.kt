package UI

import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.compose.animation.*
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavDeepLinkSaveStateControl
import com.example.cardmemoryapp.R
import kotlin.math.roundToInt

/*
    Refactor later: Start
*/
data class SettingsList(
    var deckAmountOfCards: Int = 52,
    var suits: Int = 4,
    var cardDivision: Boolean = false,
    var cardNumberOffset: Int = 0,
    var startingCardNumber: Int = 0,
)

var settings: SettingsList = SettingsList()

// Maybe make mainDeck a map and make:
// key: Resources Id
// value: drawable name(R.drawable.ic_diamond_ace)
// for clarity sake and that getting id trough resouces is much faster


var mainDeck: Array<String> = arrayOf(
    "ic_diamond_ace",
    "ic_diamond_two",
    "ic_diamond_three",
    "ic_diamond_four",
    "ic_diamond_five",
    "ic_diamond_six",
    "ic_diamond_seven",
    "ic_diamond_eight",
    "ic_diamond_nine",
    "ic_diamond_ten",
    "ic_diamond_jack",
    "ic_diamond_queen",
    "ic_diamond_king",
    "ic_club_ace",
    "ic_club_two",
    "ic_club_three",
    "ic_club_four",
    "ic_club_five",
    "ic_club_six",
    "ic_club_seven",
    "ic_club_eight",
    "ic_club_nine",
    "ic_club_ten",
    "ic_club_jack",
    "ic_club_queen",
    "ic_club_king",
    "ic_heart_ace",
    "ic_heart_two",
    "ic_heart_three",
    "ic_heart_four",
    "ic_heart_five",
    "ic_heart_six",
    "ic_heart_seven",
    "ic_heart_eight",
    "ic_heart_nine",
    "ic_heart_ten",
    "ic_heart_jack",
    "ic_heart_queen",
    "ic_heart_king",
    "ic_spade_ace",
    "ic_spade_two",
    "ic_spade_three",
    "ic_spade_four",
    "ic_spade_five",
    "ic_spade_six",
    "ic_spade_seven",
    "ic_spade_eight",
    "ic_spade_nine",
    "ic_spade_ten",
    "ic_spade_jack",
    "ic_spade_queen",
    "ic_spade_king",
)

/*
var mainDeck: Array<String> = arrayOf(
    "R.drawable.ic_diamond_ace",
    "R.drawable.ic_diamond_two",
    "R.drawable.ic_diamond_three",
    "R.drawable.ic_diamond_four",
    "R.drawable.ic_diamond_five",
    "R.drawable.ic_diamond_six",
    "R.drawable.ic_diamond_seven",
    "R.drawable.ic_diamond_eight",
    "R.drawable.ic_diamond_nine",
    "R.drawable.ic_diamond_ten",
    "R.drawable.ic_diamond_jack",
    "R.drawable.ic_diamond_queen",
    "R.drawable.ic_diamond_king",
    "R.drawable.ic_club_ace",
    "R.drawable.ic_club_two",
    "R.drawable.ic_club_three",
    "R.drawable.ic_club_four",
    "R.drawable.ic_club_five",
    "R.drawable.ic_club_six",
    "R.drawable.ic_club_seven",
    "R.drawable.ic_club_eight",
    "R.drawable.ic_club_nine",
    "R.drawable.ic_club_ten",
    "R.drawable.ic_club_jack",
    "R.drawable.ic_club_queen",
    "R.drawable.ic_club_king",
    "R.drawable.ic_heart_ace",
    "R.drawable.ic_heart_two",
    "R.drawable.ic_heart_three",
    "R.drawable.ic_heart_four",
    "R.drawable.ic_heart_five",
    "R.drawable.ic_heart_six",
    "R.drawable.ic_heart_seven",
    "R.drawable.ic_heart_eight",
    "R.drawable.ic_heart_nine",
    "R.drawable.ic_heart_ten",
    "R.drawable.ic_heart_jack",
    "R.drawable.ic_heart_queen",
    "R.drawable.ic_heart_king",
    "R.drawable.ic_spade_ace",
    "R.drawable.ic_spade_two",
    "R.drawable.ic_spade_three",
    "R.drawable.ic_spade_four",
    "R.drawable.ic_spade_five",
    "R.drawable.ic_spade_six",
    "R.drawable.ic_spade_seven",
    "R.drawable.ic_spade_eight",
    "R.drawable.ic_spade_nine",
    "R.drawable.ic_spade_ten",
    "R.drawable.ic_spade_jack",
    "R.drawable.ic_spade_queen",
    "R.drawable.ic_spade_king",
)
*/


fun deckGeneration(deckToRandomize: IntArray): IntArray {
    var mainDeckKey: IntArray = IntArray(52) {it}
    mainDeckKey.shuffle()

    return mainDeckKey
}


enum class SwipingDirection{
    NONE, RIGHTTOLEFT, LEFTORIGHT
}

/*
    Refactor later: End
*/






@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun DealtCardScreen(navController: NavController) {


    //todo change width to feet any screen
    val width = 350.dp

    val squareSize = 50.dp
    //val swipeableState = rememberSwipeableState("ic_spade_queen")
    val scrollState = rememberScrollState()
    val sizePx = with(LocalDensity.current) { (width - squareSize).toPx() }
    val anchors = mapOf(0f to "ic_spade_queen", -10f to "ic_spade_king")

    val resources = LocalContext.current.resources
    //val w = resources.getIdentifier("ic_spade_queen")




    //Kopijuotas kodas
    /*
    val connection = remember {
        object : NestedScrollConnection {

            override fun onPreScroll(
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                val delta = available.y
                return if (delta < 0) {
                    swipeableState.performDrag(delta).toOffset()
                } else {
                    Offset.Zero
                }
            }

            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                val delta = available.y
                return swipeableState.performDrag(delta).toOffset()
            }

            override suspend fun onPreFling(available: Velocity): Velocity {
                return if (available.y < 0 && scrollState.value == 0) {
                    swipeableState.performFling(available.y)
                    available
                } else {
                    Velocity.Zero
                }
            }

            override suspend fun onPostFling(
                consumed: Velocity,
                available: Velocity
            ): Velocity {
                swipeableState.performFling(velocity = available.y)
                return super.onPostFling(consumed, available)
            }

            private fun Float.toOffset() = Offset(0f, this)
        }
    }*/
    // kopijuoto kodo pabaiga


    //fuck

    /*
    var visible by remember { mutableStateOf(true) }

    val density = LocalDensity.current

    AnimatedVisibility(
        visible = visible,
        enter = slideInVertically()
    ) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .swipeable(
                state = swipeableState,
                anchors = anchors,
                thresholds = { _, _ -> FractionalThreshold(1f) },
                orientation = Orientation.Horizontal
            )//.nestedScroll(connection)
            .background(Color.Black)
    ) {


        Image(
            painterResource(resources.getIdentifier(swipeableState.currentValue,"drawable", LocalContext.current.getPackageName())),
            //painterResource(R.drawable.ic_spade_king),
            contentDescription = null,
            modifier = Modifier.fillMaxSize().
            offset { IntOffset(swipeableState.offset.value.roundToInt(), 0) }
        )
        }
    }
    */
/*
    Box(
        Modifier
            .offset { IntOffset(swipeableState.offset.value.roundToInt(), 0) }
            .size(squareSize)
            .background(Color.Red),
        contentAlignment = Alignment.Center
    ) {
        Text(swipeableState.currentValue, color = Color.White, fontSize = 24.sp)
    }
*/
/*
    Image(
        painterResource(R.drawable.ic_diamond_jack),
        contentDescription = null,
        modifier = Modifier.fillMaxSize().swipeable(
            state = swipeableState,
            anchors = anchors,
            thresholds = { _, _ -> FractionalThreshold(0.5f) },
            orientation = Orientation.Horizontal
        )
    )*/

    // just no

    var swipingDirection by remember{mutableStateOf(SwipingDirection.NONE)}

    val offsetX = remember { mutableStateOf(0f) }
    val offsetY = remember { mutableStateOf(0f) }
    //var size by remember { mutableStateOf(Size.Zero) }
    var color by remember { mutableStateOf(Color.Green) }
    var swipeableState by remember {mutableStateOf("ic_spade_queen")}

    fun screenSwipe(swipingDirection: SwipingDirection,
        currentCardPlace: Int,
        cardsImageRoute: Array<String>,
        swipeableState: String
    ) {
        // priklausomai nuo swipinimo i kuria puse reikia pritaikyti animacija
    /* So it seams that you can change the given in function variable value
        swipeableState = when(swipingDirection) {
            SwipingDirection.LEFTORIGHT -> cardsImageRoute[0]
            SwipingDirection.RIGHTTOLEFT -> cardsImageRoute[2]
            SwipingDirection.NONE -> swipeableState
        }
    */
        //secondary algo


        // Main algo

        // Getting direction
        // Getting card on the screen(resources and card place in the deck)
        // Getting card on the sides
        // Deciding which card to display
        // the last and first cards should have difrenet animation
        // Animation

    }

    var testDeck: Array<String> = arrayOf(
        "ic_diamond_ace",
        "ic_diamond_two",
        "ic_diamond_three")

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .pointerInput(Unit){
                detectDragGestures (
                    onDragEnd = {
                        screenSwipe(swipingDirection,
                        3,
                        testDeck,
                        swipeableState
                        )
                    }
                        ) { _, dragAmount ->


                    val original = Offset(offsetX.value, offsetY.value)
                    val summed = original + dragAmount
                    val newValue = Offset(
                        x = summed.x.coerceIn(0f, size.width - 50.dp.toPx()),
                        y = summed.y.coerceIn(0f, size.height - 50.dp.toPx())
                    )
                    offsetX.value = newValue.x
                    offsetY.value = newValue.y

                    when{
                        dragAmount.x > 0 -> swipingDirection = SwipingDirection.LEFTORIGHT
                        dragAmount.x < 0 -> swipingDirection = SwipingDirection.RIGHTTOLEFT
                        else -> swipingDirection = SwipingDirection.NONE
                    }

                    swipeableState = when{
                        dragAmount.x > 0 -> testDeck[0]
                        dragAmount.x < 0 -> testDeck[2]
                        else -> testDeck[1]
                    }
                }
            }
            .background(color)
        )
    {
        Image(
            painterResource(resources.getIdentifier(swipeableState,"drawable", LocalContext.current.getPackageName())),
            //painterResource(R.drawable.ic_spade_king),
            contentDescription = null,
            modifier = Modifier.fillMaxSize()
        )
    }



    // should go to a screen to chose saved cards or to create a new deck fo review
    // In review mode you choose suit and a card number and the card is displayed for you
    // val deckValuess
    // random deck generation
    // depending on settings you should get diffrent amount of cards
    // show card
    // swipe left to next card or right for previous
    // after the last card should give a possibility to repeat
    // Should be a setting where you choose if the user has chosen 14 cards will it be devided
    // equaly by the amount of suits or make one suit full and other how many is left
    // should suits numbers be random or periodic from a starting card
    // Choosing starting card should be a drop down menu() in which the card you can choose from
    // which to start should be given only after

}