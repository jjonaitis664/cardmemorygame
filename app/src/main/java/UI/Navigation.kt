package UI

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable

fun Navigation () {
    val navController = rememberNavController()

    NavHost(navController = navController,
        startDestination = ScreenRoutes.MainMenuScreen.route
    ) {
        composable(ScreenRoutes.MainMenuScreen.route) {
            MainMenuScreen(navController =  navController)
        }
        composable(ScreenRoutes.DealtCardScreen .route) {
            DealtCardScreen(navController =  navController)
        }
        composable(ScreenRoutes.SavedDecksScreen.route) {
            SavedDecksScreen(navController =  navController)
        }
        composable(ScreenRoutes.SettingsScreen.route) {
            SettingsScreen(navController =  navController)
        }
    }
}