//import data/ProtoSettingsRepository

package logicLayer

enum class Suits {
    HEART,
    SPADES,
    CLUBS,
    DIAMONDS
}

enum class StartingCardSettings{
    DEFAULT,
    STARTING_CARD_NUMBER,
    RANDOM
}

enum class CardValues {
    ACE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING
}

data class PlayingCard(
    val suits: Suits,
    val number: Number
)

class Deck(
    val amountOfCards: Int,
    val suits: Array<Suits>,
    val cardDivision: Boolean,//Full suit or add devide equaly by all cards
    val startingCardNumber: StartingCardSettings = StartingCardSettings.DEFAULT, //division by beeing periodic or not
) {
    init{
        println("W")
    }

    fun basicDeckGeneration() {
        // amount of cards  = 52
        // suits = [Clubs,Diamond,Spades,Heart]
        // cardDivision True
        // Starting Card number = 0

        //if(startingCardNumber = 0)

        //if cardDivision is true
        val suitsAmount = suits.size
        val cardsAmountInSuit: Int = amountOfCards / suitsAmount
        //else(startingCardNumber)

        //if(startingCardNumber)

        when (startingCardNumber) {
            StartingCardSettings.DEFAULT -> {
                println("wwwww")
            }
            StartingCardSettings.STARTING_CARD_NUMBER -> {
                println("ww")
            }
            StartingCardSettings.RANDOM -> {
                println("www")
            }
        }
    }
}






/*
fun showCard(card: PlayingCard): Int {
    val map = mapOf(
        Suits.HEART to mapOf(
            Number.ACE to 1,
            Number.TWO to 2
        )

    return map[card.suits]?.get(card.number) :? throw Illegal
    )
}*/

/*
fun cardsImagePath(card: PlayingCard): Int{
    return when (card.suits) {
        Suits.HEART -> {
            when (card.number) {
                Number.ACE -> 1,
                Number.TWO -> 2
            }
        }
    }
}
*/
